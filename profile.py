"""Spectrum testing
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import IG extensions
import geni.rspec.igext as ig
# Emulab specific extensions.
import geni.rspec.emulab as emulab
# Spectrum specific extensions.
import geni.rspec.emulab.spectrum as spectrum
# Route specific extensions.
import geni.rspec.emulab.route as route
# RDZ extensions
import geni.rspec.emulab.rdz as rdz

# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

def findName(list, key):
    for pair in list:
        if pair[0] == key:
            return pair[1]
        pass
    return None

# A list of endpoint sites.
endpoints = [
    ('', "None"),
    ('urn:publicid:IDN+test1.powderwireless.net+authority+cm',
     "Prototype"),
    ('urn:publicid:IDN+bookstore.powderwireless.net+authority+cm',
     "Bookstore"),
    ('urn:publicid:IDN+cpg.powderwireless.net+authority+cm',
     "CPG"),
    ('urn:publicid:IDN+ebc.powderwireless.net+authority+cm',
     "EBC"),
    ('urn:publicid:IDN+guesthouse.powderwireless.net+authority+cm',
     "Guesthouse"),
    ('urn:publicid:IDN+humanities.powderwireless.net+authority+cm',
     "Humanities"),
    ('urn:publicid:IDN+law73.powderwireless.net+authority+cm',
     "Law"),
    ('urn:publicid:IDN+madsen.powderwireless.net+authority+cm',
     "Madsen"),
    ('urn:publicid:IDN+moran.powderwireless.net+authority+cm',
     "Moran"),
    ('urn:publicid:IDN+sagepoint.powderwireless.net+authority+cm',
     "Sagepoint"),
    ('urn:publicid:IDN+web.powderwireless.net+authority+cm',
     "WEB")]

# And individual buses.
buses = [
    ('', "None"),
    ('urn:publicid:IDN+bus-test2.powderwireless.net+authority+cm',
     "Bus Test2"),
    ('urn:publicid:IDN+bus-build.powderwireless.net+authority+cm',
     "Bus Build"),
    ('urn:publicid:IDN+bus-4208.powderwireless.net+authority+cm',
     "Bus 4208"),
    ('urn:publicid:IDN+bus-4329.powderwireless.net+authority+cm',
     "Bus 4329"),
    ('urn:publicid:IDN+bus-4330.powderwireless.net+authority+cm',
     "Bus 4330"),
    ('urn:publicid:IDN+bus-4407.powderwireless.net+authority+cm',
     "Bus 4407"),
    ('urn:publicid:IDN+bus-4408.powderwireless.net+authority+cm',
     "Bus 4408"),
    ('urn:publicid:IDN+bus-4409.powderwireless.net+authority+cm',
     "Bus 4409"),
    ('urn:publicid:IDN+bus-4410.powderwireless.net+authority+cm',
     "Bus 4410"),
    ('urn:publicid:IDN+bus-4555.powderwireless.net+authority+cm',
     "Bus 4555"),
    ('urn:publicid:IDN+bus-4603.powderwireless.net+authority+cm',
     "Bus 4603"),
    ('urn:publicid:IDN+bus-4604.powderwireless.net+authority+cm',
     "Bus 4604"),
    ('urn:publicid:IDN+bus-4734.powderwireless.net+authority+cm',
     "Bus 4734"),
    ('urn:publicid:IDN+bus-4817.powderwireless.net+authority+cm',
     "Bus 4817"),
    ('urn:publicid:IDN+bus-4964.powderwireless.net+authority+cm',
     "Bus 4964"),
    ('urn:publicid:IDN+bus-5175.powderwireless.net+authority+cm',
     "Bus 5175"),
    ('urn:publicid:IDN+bus-6180.powderwireless.net+authority+cm',
     "Bus 6180"),
    ('urn:publicid:IDN+bus-6181.powderwireless.net+authority+cm',
     "Bus 6181"),
    ('urn:publicid:IDN+bus-6182.powderwireless.net+authority+cm',
     "Bus 6182"),
    ('urn:publicid:IDN+bus-6183.powderwireless.net+authority+cm',
     "Bus 6183"),
    ('urn:publicid:IDN+bus-6185.powderwireless.net+authority+cm',
     "Bus 6185"),
    ('urn:publicid:IDN+bus-6186.powderwireless.net+authority+cm',
     "Bus 6186"),
    ('urn:publicid:IDN+bus-6360.powderwireless.net+authority+cm',
     "Bus 6360"),
    ('urn:publicid:IDN+bus-6362.powderwireless.net+authority+cm',
     "Bus 6362"),
    ('urn:publicid:IDN+bus-6363.powderwireless.net+authority+cm',
     "Bus 6363"),
    ('urn:publicid:IDN+bus-6364.powderwireless.net+authority+cm',
     "Bus 6364"),
    ('urn:publicid:IDN+bus-6365.powderwireless.net+authority+cm',
     "Bus 6365"),
    ]

# A list of radios on the Motherwhip.
radios = [
    ('', "None"),
    ('cbrssdr1-bes', 'cbrssdr1-bes'),
    ('cbrssdr1-browning','cbrssdr1-browning'),
    ('cbrssdr1-dentistry','cbrssdr1-dentistry'),
    ('cbrssdr1-fm','cbrssdr1-fm'),
    ('cbrssdr1-honors','cbrssdr1-honors'),
    ('cbrssdr1-hospital','cbrssdr1-hospital'),
    ('cbrssdr1-meb','cbrssdr1-meb'),
    ('cbrssdr1-smt','cbrssdr1-smt'),
    ('cbrssdr1-ustar','cbrssdr1-ustar'),
    ('cellsdr1-bes','cellsdr1-bes'),
    ('cellsdr1-browning','cellsdr1-browning'),
    ('cellsdr1-dentistry','cellsdr1-dentistry'),
    ('cellsdr1-fm','cellsdr1-fm'),
    ('cellsdr1-honors','cellsdr1-honors'),
    ('cellsdr1-hospital','cellsdr1-hospital'),
    ('cellsdr1-meb','cellsdr1-meb'),
    ('cellsdr1-smt','cellsdr1-smt'),
    ('cellsdr1-ustar','cellsdr1-ustar'),
    ('cnode-ebc','cnode-ebc'),
    ('cnode-guesthouse','cnode-guesthouse'),
    ('cnode-mario','cnode-mario'),
    ('cnode-moran','cnode-moran'),
    ('cnode-ustar','cnode-ustar'),
    ('cnode-wasatch','cnode-wasatch'),
    ('ota-bru','ota-bru'),
    ('ota-nuc1','ota-nuc1'),
    ('ota-nuc2','ota-nuc2'),
    ('ota-nuc3','ota-nuc3'),
    ('ota-nuc4','ota-nuc4'),
    ('ota-x310-1','ota-x310-1'),
    ('ota-x310-2','ota-x310-2'),
    ('ota-x310-3','ota-x310-3'),
    ('ota-x310-4','ota-x310-4'),
    ('oai-wb-a1','oai-wb-a1'),
    ('oai-wb-a2','oai-wb-a2'),
    ('oai-wb-b1','oai-wb-b1'),
    ('oai-wb-b1','oai-wb-b1'),
    ]

# A list of routes
routes = [
    ('',                  "None"),
    ('allroutes',         "All"),
    ("Blue Detour",       "Blue Detour"),
    ("Guardsman Direct",  "Guardsman Direct"),
    ('Red Detour',        "Red Detour"),
    ("Orange",            "Orange"),
    ("Wasatch Express",   "Wasatch Express"),
    ("Circulator",        "Circulator"),
    ]

# Request a BS radio (with associated compute node).
pc.defineParameter("Radios", "Radio",
                   portal.ParameterType.STRING, [radios[0][0]], radios,
                   min=1, multiValue=1, itemDefaultValue='')

# Request a route
pc.defineParameter("Routes", "Bus Route",
                   portal.ParameterType.STRING, [routes[0][0]], routes,
                   min=1, multiValue=1, itemDefaultValue='')

# Request a specific bus
pc.defineParameter("Buses", "Bus",
                   portal.ParameterType.STRING, [buses[0][0]], buses,
                   min=1, multiValue=1, itemDefaultValue='')

pc.defineParameter("FreqLow", "Frequency Min",
                   portal.ParameterType.BANDWIDTH, 0,
                   longDescription="This range is added to the radios, routes, and " +
                   "buses above. Values are rounded to the nearest kilohertz.")

pc.defineParameter("FreqHigh", "Frequency Max",
                   portal.ParameterType.BANDWIDTH, 0,
                   longDescription="This range is added to the radios, routes, and " +
                   "buses above. Values are rounded to the nearest kilohertz.")

pc.defineParameter("FreqWidth", "Frequency Width",
                   portal.ParameterType.BANDWIDTH, 0,
                   longDescription="This is an RDZ specific option.")

# Add an optional compute node on the Mothership
pc.defineParameter("ComputeNode",  "Add a compute node",
                   portal.ParameterType.BOOLEAN, False,
                   longDescription="Add an Emulab compute node (d710).")

pc.defineParameter("useRDZ","Inner RDZ",
                   portal.ParameterType.STRING, "",
                   longDescription="Provide the name of the RDZinRDZ experiment to use for this experiment")

# Structure to add endpoints.
ps = pc.defineStructParameter(
    "Endpoints","Endpoint",[],
    multiValue=True,min=1,hide=False,
    multiValueTitle="Fixed Endpoints",
    members=[portal.Parameter("FE","Fixed Endpoint",
                              portal.ParameterType.STRING, endpoints[0], endpoints),
             portal.Parameter("node_id", "Radio ID",
                              portal.ParameterType.STRING, "nuc2"),
             portal.Parameter("lower","Frequency Min",
                              portal.ParameterType.BANDWIDTH, 0,
                              longDescription="Values are rounded to the nearest kilohertz."),
             portal.Parameter("upper", "Frequency Max",
                              portal.ParameterType.BANDWIDTH, 0,
                              longDescription="Values are rounded to the nearest kilohertz."),
             portal.Parameter("width", "Frequency Width",
                              portal.ParameterType.BANDWIDTH, 0,
                              longDescription="This is an RDZ specific option."),
             ])

# Now a structure to define a set of frequency ranges
ps = pc.defineStructParameter(
    "Spectrum","Range",[{"lower" : "3701.3", "upper" : "3703.7"}],
    multiValue=True,min=0,hide=False,
    multiValueTitle="Global frequency range declarations for over-the-air operation.",
    members=[portal.Parameter("lower","Frequency Min",
                              portal.ParameterType.BANDWIDTH, 0,
                              longDescription="Values are rounded to the nearest kilohertz."),
             portal.Parameter("upper", "Frequency Max",
                              portal.ParameterType.BANDWIDTH, 0,
                              longDescription="Values are rounded to the nearest kilohertz."),
             portal.Parameter("width", "Frequency Width",
                              portal.ParameterType.BANDWIDTH, 0,
                              longDescription="This is an RDZ specific option."),
             ])

# Retrieve the values the user specifies during instantiation.
params = pc.bindParameters()

# Check parameter validity.
#if params.node_id.strip() == "":
#    pc.reportError(portal.ParameterError("You must specify a node.", ["node_id"]))
#if params.iface.strip() == "":
#    pc.reportError(portal.ParameterError("You must specify an interface.", ["iface"]))
#if False and params.n and params.phystype == "":
#    pc.reportError(portal.ParameterError("You must specify a type for compute nodes", ["phystype"]))

# Multivalue field errors must reference by index. 
index       = 0 

for range in params.Spectrum:
    if range.lower == 0:
        pc.reportError(portal.ParameterError("You must specify lower frequency",
                                             [ 'Spectrum[%d].lower' % (index) ]))
        pass
    if range.upper == 0:
        pc.reportError(portal.ParameterError("You must specify an upper frequency",
                                             [ 'Spectrum[%d].upper' % (index) ]))
        pass
    index = index + 1
    pass
    
# Throw the errors back to the user.
pc.verifyParameters()

#
# Radios that require an associate compute node.
#
for radioname in params.Radios:
    if radioname != "":
        radio = request.RawPC(radioname)
        radio.component_id = radioname
        radio.component_manager_id = 'urn:publicid:IDN+emulab.net+authority+cm'
        radioiface = radio.addInterface();

        if params.FreqLow and params.FreqHigh:
            radio.requestSpectrum(params.FreqLow, params.FreqHigh, 0, params.FreqWidth)
            pass

        if radioname.startswith("cnode"):
            continue;
        
        # Always need a compute node with a link to the radio.
        com = request.RawPC("com-" + radioname)
        com.component_manager_id = 'urn:publicid:IDN+emulab.net+authority+cm'
        com.hardware_type = "d430"
        com.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
        comiface = com.addInterface();
        com.addService(pg.Execute(shell="sh", command="sudo /bin/bash /local/repository/install.sh"))
        com.startVNC()
    
        if (radioname.startswith("cbrs") or radioname.startswith("cell") or
            radioname.startswith("ota") or radioname.startswith("oai")):
            radioiface.addAddress(pg.IPv4Address("192.168.40.2", "255.255.255.0"))
            comiface.addAddress(pg.IPv4Address("192.168.40.1", "255.255.255.0"))
        else:
            radioiface.addAddress(pg.IPv4Address("192.168.10.2", "255.255.255.0"))
            comiface.addAddress(pg.IPv4Address("192.168.10.1", "255.255.255.0"))
            pass
        link = request.Link(members = [radioiface, comiface])
        pass
    pass

#
# Add Fixed Endpoints
#
for endpoint in params.Endpoints:
    if endpoint.FE != "":
        # Add a raw PC to the request
        node = request.RawPC(findName(endpoints, endpoint.FE) + "-" + endpoint.node_id)
        if endpoint.node_id != "any":
            node.component_id = endpoint.node_id
            pass
        node.component_manager_id = endpoint.FE
        node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
        node.startVNC()
        node.addService(pg.Execute(shell="sh",
                                   command="sudo /bin/bash /local/repository/install.sh"))

        if endpoint.lower and endpoint.upper:
            node.requestSpectrum(endpoint.lower, endpoint.upper, 0, endpoint.width)
            pass
        pass
    pass

#
# Individual buses
#
for bus in params.Buses:
    if bus != "":
        # Add a raw PC to the request
        name = findName(buses, bus).replace(" ", "-")
        node = request.RawPC(name)
        node.component_id = "ed1"
        node.component_manager_id = bus
        node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
        node.startVNC()
        node.addService(pg.Execute(shell="sh",
                                   command="sudo /bin/bash /local/repository/install.sh"))
        if params.FreqLow and params.FreqHigh:
            node.requestSpectrum(params.FreqLow, params.FreqHigh, 0, params.FreqWidth)
            pass

        pass
    pass

#
# Ranges
#
for range in params.Spectrum:
    # Request spectrum to use on a specific interface on the node. In MHZ
    # First arg is frequency low bound, second is high bound, third is max power.
    #iface.requestSpectrum(params.lower, params.upper, 0)
    
    # Request to use spectrum on any TX devices attached to the node. 
    #node.requestSpectrum(params.lower, params.upper, 0)
    #node.requestSpectrum(params.lower, params.upper, 0)
    
    # Request spectrum to use across the all nodes in the experiment.
    request.requestSpectrum(range.lower, range.upper, 0, range.width)
    pass

for route in params.Routes:
    if route != "":
        busroute = request.requestBusRoute(route)
        if params.FreqLow and params.FreqHigh:
            busroute.requestSpectrum(params.FreqLow, params.FreqHigh, 0, params.FreqWidth)
            pass
        pass
    pass

#
# Extra compute node.
#
if params.ComputeNode:
    com = request.RawPC("compute")
    com.component_manager_id = 'urn:publicid:IDN+emulab.net+authority+cm'
    com.hardware_type = "d710"
    com.startVNC()
    pass

# Flag as needing a specific RDZ
if params.useRDZ != "":
    request.useRDZ(instance=params.useRDZ);
    pass

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
