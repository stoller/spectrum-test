"""Just for running uhd_fft
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import IG extensions
import geni.rspec.igext as ig
# Emulab specific extensions.
import geni.rspec.emulab as emulab
# Spectrum specific extensions.
import geni.rspec.emulab.spectrum as spectrum

# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

def findName(list, key):
    for pair in list:
        if pair[0] == key:
            return pair[1]
        pass
    return None

# A list of endpoint sites.
endpoints = [
    ('', "None"),
    ('urn:publicid:IDN+test1.powderwireless.net+authority+cm',
     "Prototype"),
    ('urn:publicid:IDN+bookstore.powderwireless.net+authority+cm',
     "Bookstore"),
    ('urn:publicid:IDN+cpg.powderwireless.net+authority+cm',
     "CPG"),
    ('urn:publicid:IDN+ebc.powderwireless.net+authority+cm',
     "EBC"),
    ('urn:publicid:IDN+guesthouse.powderwireless.net+authority+cm',
     "Guesthouse"),
    ('urn:publicid:IDN+humanities.powderwireless.net+authority+cm',
     "Humanities"),
    ('urn:publicid:IDN+law73.powderwireless.net+authority+cm',
     "Law"),
    ('urn:publicid:IDN+madsen.powderwireless.net+authority+cm',
     "Madsen"),
    ('urn:publicid:IDN+moran.powderwireless.net+authority+cm',
     "Moran"),
    ('urn:publicid:IDN+sagepoint.powderwireless.net+authority+cm',
     "Sagepoint"),
    ('urn:publicid:IDN+web.powderwireless.net+authority+cm',
     "WEB")]

# And individual buses.
buses = [
    ('', "None"),
    ('urn:publicid:IDN+bus-test2.powderwireless.net+authority+cm',
     "Bus Test2"),
    ('urn:publicid:IDN+bus-build.powderwireless.net+authority+cm',
     "Bus Build"),
    ('urn:publicid:IDN+bus-4208.powderwireless.net+authority+cm',
     "Bus 4208"),
    ('urn:publicid:IDN+bus-4329.powderwireless.net+authority+cm',
     "Bus 4329"),
    ('urn:publicid:IDN+bus-4330.powderwireless.net+authority+cm',
     "Bus 4330"),
    ('urn:publicid:IDN+bus-4407.powderwireless.net+authority+cm',
     "Bus 4407"),
    ('urn:publicid:IDN+bus-4408.powderwireless.net+authority+cm',
     "Bus 4408"),
    ('urn:publicid:IDN+bus-4409.powderwireless.net+authority+cm',
     "Bus 4409"),
    ('urn:publicid:IDN+bus-4410.powderwireless.net+authority+cm',
     "Bus 4410"),
    ('urn:publicid:IDN+bus-4555.powderwireless.net+authority+cm',
     "Bus 4555"),
    ('urn:publicid:IDN+bus-4603.powderwireless.net+authority+cm',
     "Bus 4603"),
    ('urn:publicid:IDN+bus-4604.powderwireless.net+authority+cm',
     "Bus 4604"),
    ('urn:publicid:IDN+bus-4734.powderwireless.net+authority+cm',
     "Bus 4734"),
    ('urn:publicid:IDN+bus-4817.powderwireless.net+authority+cm',
     "Bus 4817"),
    ('urn:publicid:IDN+bus-4964.powderwireless.net+authority+cm',
     "Bus 4964"),
    ('urn:publicid:IDN+bus-5175.powderwireless.net+authority+cm',
     "Bus 5175"),
    ('urn:publicid:IDN+bus-6180.powderwireless.net+authority+cm',
     "Bus 6180"),
    ('urn:publicid:IDN+bus-6181.powderwireless.net+authority+cm',
     "Bus 6181"),
    ('urn:publicid:IDN+bus-6182.powderwireless.net+authority+cm',
     "Bus 6182"),
    ('urn:publicid:IDN+bus-6183.powderwireless.net+authority+cm',
     "Bus 6183"),
    ('urn:publicid:IDN+bus-6185.powderwireless.net+authority+cm',
     "Bus 6185"),
    ('urn:publicid:IDN+bus-6186.powderwireless.net+authority+cm',
     "Bus 6186"),
    ('urn:publicid:IDN+bus-6360.powderwireless.net+authority+cm',
     "Bus 6360"),
    ('urn:publicid:IDN+bus-6362.powderwireless.net+authority+cm',
     "Bus 6362"),
    ('urn:publicid:IDN+bus-6363.powderwireless.net+authority+cm',
     "Bus 6363"),
    ('urn:publicid:IDN+bus-6364.powderwireless.net+authority+cm',
     "Bus 6364"),
    ('urn:publicid:IDN+bus-6365.powderwireless.net+authority+cm',
     "Bus 6365"),
    ]

# A list of radios on the Motherwhip.
radios = [
    ('', "None"),
    ('cbrssdr1-bes', 'cbrssdr1-bes'),
    ('cbrssdr1-browning','cbrssdr1-browning'),
    ('cbrssdr1-dentistry','cbrssdr1-dentistry'),
    ('cbrssdr1-fm','cbrssdr1-fm'),
    ('cbrssdr1-honors','cbrssdr1-honors'),
    ('cbrssdr1-hospital','cbrssdr1-hospital'),
    ('cbrssdr1-meb','cbrssdr1-meb'),
    ('cbrssdr1-smt','cbrssdr1-smt'),
    ('cbrssdr1-ustar','cbrssdr1-ustar'),
    ('cellsdr1-bes','cellsdr1-bes'),
    ('cellsdr1-browning','cellsdr1-browning'),
    ('cellsdr1-dentistry','cellsdr1-dentistry'),
    ('cellsdr1-fm','cellsdr1-fm'),
    ('cellsdr1-honors','cellsdr1-honors'),
    ('cellsdr1-hospital','cellsdr1-hospital'),
    ('cellsdr1-meb','cellsdr1-meb'),
    ('cellsdr1-smt','cellsdr1-smt'),
    ('cellsdr1-ustar','cellsdr1-ustar'),
    ('cnode-ebc','cnode-ebc'),
    ('cnode-guesthouse','cnode-guesthouse'),
    ('cnode-mario','cnode-mario'),
    ('cnode-moran','cnode-moran'),
    ('cnode-ustar','cnode-ustar'),
    ('cnode-wasatch','cnode-wasatch'),
    ('ota-bru','ota-bru'),
    ('ota-nuc1','ota-nuc1'),
    ('ota-nuc2','ota-nuc2'),
    ('ota-nuc3','ota-nuc3'),
    ('ota-nuc4','ota-nuc4'),
    ('ota-x310-1','ota-x310-1'),
    ('ota-x310-2','ota-x310-2'),
    ('ota-x310-3','ota-x310-3'),
    ('ota-x310-4','ota-x310-4'),
    ('oai-wb-a1','oai-wb-a1'),
    ('oai-wb-a2','oai-wb-a2'),
    ('oai-wb-b1','oai-wb-b1'),
    ('oai-wb-b1','oai-wb-b1'),
    ]

# Request a BS radio (with associated compute node).
pc.defineParameter("Radios", "Radio",
                   portal.ParameterType.STRING, [radios[0][0]], radios,
                   min=1, multiValue=1, itemDefaultValue='')

# Structure to add endpoints.
ps = pc.defineStructParameter(
    "Endpoints","Endpoint",[],
    multiValue=True,min=1,hide=False,
    multiValueTitle="Fixed Endpoints",
    members=[portal.Parameter("FE","Fixed Endpoint",
                              portal.ParameterType.STRING, endpoints[0], endpoints),
             portal.Parameter("node_id", "Radio ID",
                              portal.ParameterType.STRING, "nuc1"),
             ])

# Now a structure to define a set of frequency ranges
# Retrieve the values the user specifies during instantiation.
params = pc.bindParameters()

# Multivalue field errors must reference by index. 
index       = 0 

# Throw the errors back to the user.
pc.verifyParameters()

#
# Radios that require an associate compute node.
#
for radioname in params.Radios:
    if radioname != "":
        radio = request.RawPC(radioname)
        radio.component_id = radioname
        radio.component_manager_id = 'urn:publicid:IDN+emulab.net+authority+cm'
        radioiface = radio.addInterface();

        if radioname.startswith("cnode"):
            continue;
        
        # Always need a compute node with a link to the radio.
        com = request.RawPC("com-" + radioname)
        com.component_manager_id = 'urn:publicid:IDN+emulab.net+authority+cm'
        com.hardware_type = "d430"
        com.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
        comiface = com.addInterface();
        com.addService(pg.Execute(shell="sh", command="sudo /bin/bash /local/repository/install.sh"))
        com.startVNC()
    
        if (radioname.startswith("cbrs") or radioname.startswith("cell") or
            radioname.startswith("ota") or radioname.startswith("oai")):
            radioiface.addAddress(pg.IPv4Address("192.168.40.2", "255.255.255.0"))
            comiface.addAddress(pg.IPv4Address("192.168.40.1", "255.255.255.0"))
        else:
            radioiface.addAddress(pg.IPv4Address("192.168.10.2", "255.255.255.0"))
            comiface.addAddress(pg.IPv4Address("192.168.10.1", "255.255.255.0"))
            pass
        link = request.Link(members = [radioiface, comiface])
        pass
    pass

#
# Add Fixed Endpoints
#
for endpoint in params.Endpoints:
    if endpoint.FE != "":
        # Add a raw PC to the request
        node = request.RawPC(findName(endpoints, endpoint.FE) + "-" + endpoint.node_id)
        if endpoint.node_id != "any":
            node.component_id = endpoint.node_id
            pass
        node.component_manager_id = endpoint.FE
        node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
        node.startVNC()
        node.addService(pg.Execute(shell="sh",
                                   command="sudo /bin/bash /local/repository/install.sh"))
        pass
    pass

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
