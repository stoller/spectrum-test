#!/usr/bin/python

"""
Allocate a XEN VM on a specific dense cnode
"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab as emulab

INSTALL = "sudo /bin/bash /local/repository/install.sh"

request = portal.context.makeRequestRSpec()

wasatch = request.XenVM("wasatch")
wasatch.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD";
wasatch.cores = 1
wasatch.ram = 4096
wasatch.exclusive = False
wasatch.component_id = "cnode-wasatch"
wasatch.xen_ptype = "nuvo7501-vm"
wasatch.addService(rspec.Execute(shell="sh", command=INSTALL))
wasatch.Attribute("XEN_FORCE_HVM", "1")
wasatch.Attribute("XEN_USBDEVICES", "host:2500:0020")
wasatch.startVNC()

mario = request.XenVM("mario")
mario.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD";
mario.cores = 1
mario.ram = 4096
mario.exclusive = False
mario.component_id = "cnode-mario"
mario.xen_ptype = "nuvo7501-vm"
mario.addService(rspec.Execute(shell="sh", command=INSTALL))
mario.Attribute("XEN_FORCE_HVM", "1")
mario.Attribute("XEN_USBDEVICES", "host:2500:0020")
mario.startVNC()

portal.context.printRequestRSpec()
