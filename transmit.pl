#!/usr/bin/perl -w

use strict;
use Carp;
use English;
use Data::Dumper;
use Getopt::Std;
use File::Temp qw(tempfile);
use File::Basename;
use POSIX qw(isatty setsid);

# Drag in path stuff so we can find emulab stuff.
BEGIN { require "/etc/emulab/paths.pm"; import emulabpaths; }

#
# Install the monitor, run a few iterations, copy the CSV files to the
# the wbstore directory for transport back to the Mothership.
#
sub usage()
{
    print STDOUT "Usage: transmit [-i]\n";
    exit(-1);
}
my $optlist     = "i";
my $noinstall   = 0;

my $TIMEOUT     = 120; # seconds.
my $LOGFILE     = "/var/tmp/transmit.log";
my $READY       = "/etc/.txready";
my $TX          = "/usr/lib/uhd/examples/tx_waveforms";
my $REPO        = "/local/repository";
my $INSTALL     = "$REPO/install.sh";
my $FREQ        = "3550e6";
my $GAIN        = 60;
my $RATE        = "2e6";
my $ANTENNA     = "TX/RX";
my $CHANNEL     = 0;

#
# HOME will not be defined until new images are built.
#
my $HOME = $ENV{"HOME"};
if (!defined($HOME)) {
    $HOME = "/users/geniuser";
    $ENV{"HOME"} = $HOME;
    $ENV{"USER"} = "geniuser";
}

# Protos
sub fatal($);

#
# Parse command arguments.
#
my %options = ();
if (! getopts($optlist, \%options)) {
    usage();
}
if (defined($options{"i"})) {
    $noinstall = 1;
}

#
# Save off our output when not interactive, so that we can send it
# someplace useful. 
#
if (! -t) {
    open(STDOUT, "> $LOGFILE") or
	die("opening $LOGFILE for STDOUT: $!");
    open(STDERR, ">> $LOGFILE") or
	die("opening $LOGFILE for STDERR: $!");
}

# Install the monitor packages.
if (!$noinstall && ! -e "$READY") {
    system($INSTALL);
    if ($?) {
	fatal("Could not install the packages");
    }
    if (! -e "$READY") {
	fatal("Packages did not install properly");
    }
}

#
# Transmit for while then kill the transmitter and exit.
#
my $command = "$TX --ant $ANTENNA --channel $CHANNEL --rate $RATE ".
    "--freq $FREQ --gain $GAIN";

my $childpid = fork();
if ($childpid) {
    my $timedout = 0;
    local $SIG{ALRM} = sub { kill("TERM", $childpid); $timedout = 1; };
    alarm $TIMEOUT;
    waitpid($childpid, 0);
    alarm 0;
    my $stat = $? >> 8;
    if ($timedout) {
	# Normal exit
	exit(0);
    }
    exit($stat);
}
exec($command);

sub fatal($)
{
    my ($mesg) = $_[0];
    die("*** $0:\n".
	"    $mesg\n");
}

